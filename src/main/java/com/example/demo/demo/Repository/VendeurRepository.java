package com.example.demo.demo.Repository;

import com.example.demo.demo.Model.Vendeur;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface VendeurRepository extends JpaRepository<Vendeur, Long> {

    void deleteVendeurById(Long id);

    Optional<Vendeur> findVendeurById(Long id);

}
