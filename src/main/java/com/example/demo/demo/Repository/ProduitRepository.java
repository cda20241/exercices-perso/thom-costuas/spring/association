package com.example.demo.demo.Repository;

import com.example.demo.demo.Model.Produit;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface ProduitRepository extends JpaRepository<Produit, Long> {

    void deleteProduitById(Long id);

    Optional<Produit> findProduitById(Long id);

}
