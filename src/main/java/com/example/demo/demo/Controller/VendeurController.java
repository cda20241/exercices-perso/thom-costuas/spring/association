package com.example.demo.demo.Controller;

import com.example.demo.demo.Model.Vendeur;
import com.example.demo.demo.Service.VendeurService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/vendeur")
public class VendeurController {

    private final VendeurService vendeurService;

    public VendeurController(VendeurService vendeurService) {
        this.vendeurService = vendeurService;
    }

    @GetMapping("/all")
    public ResponseEntity<List<Vendeur>> getProduits() {
        List<Vendeur> vendeurs = vendeurService.findAllVendeurs();
        return new ResponseEntity<>(vendeurs, HttpStatus.OK);
    }

    @PostMapping("/add")
    public ResponseEntity<Vendeur> addProduit(@RequestBody Vendeur vendeur) {
        Vendeur newVendeur = vendeurService.addVendeur(vendeur);
        return  new ResponseEntity<>(newVendeur, HttpStatus.CREATED);
    }

    @GetMapping("find/{id}")
    public ResponseEntity<Vendeur> getProduitById(@PathVariable("id") Long id) {
        Vendeur vendeur = vendeurService.findVendeurById(id);
        return  new ResponseEntity<>(vendeur, HttpStatus.OK);
    }

    @PutMapping("/update")
    public ResponseEntity<Vendeur> updateProduit(@RequestBody Vendeur vendeur) {
        Vendeur updatedVendeur = vendeurService.updateVendeur(vendeur);
        return new ResponseEntity<>(updatedVendeur, HttpStatus.OK);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<?> deleteParent(@PathVariable("id") Long id) {
        vendeurService.deleteVendeur(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

}
