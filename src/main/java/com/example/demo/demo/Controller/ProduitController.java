package com.example.demo.demo.Controller;

import com.example.demo.demo.Model.Produit;
import com.example.demo.demo.Service.ProduitService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/produit")
public class ProduitController {

    private final ProduitService produitService;


    public ProduitController(ProduitService produitService) {
        this.produitService = produitService;
    }

    @GetMapping("/all")
    public ResponseEntity<List<Produit>> getProduits() {
        List<Produit> produits = produitService.findAllProduits();
        return new ResponseEntity<>(produits, HttpStatus.OK);
    }

    @PostMapping("/add")
    public ResponseEntity<Produit> addProduit(@RequestBody Produit produit) {
        Produit newProduit = produitService.addProduit(produit);
        return  new ResponseEntity<>(newProduit, HttpStatus.CREATED);
    }

    @GetMapping("find/{id}")
    public ResponseEntity<Produit> getProduitById(@PathVariable("id") Long id) {
        Produit produit = produitService.findProduitById(id);
        return  new ResponseEntity<>(produit, HttpStatus.OK);
    }

    @PutMapping("/update")
    public ResponseEntity<Produit> updateProduit(@RequestBody Produit produit) {
        Produit updatedProduit = produitService.updateProduit(produit);
        return new ResponseEntity<>(updatedProduit, HttpStatus.OK);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<?> deleteParent(@PathVariable("id") Long id) {
        produitService.deleteProduit(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

}
