package com.example.demo.demo.Exception;

public class ProduitNotFoundException extends RuntimeException {
    public  ProduitNotFoundException(String message) {
        super(message);
    }
}
