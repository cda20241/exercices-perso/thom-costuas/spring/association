package com.example.demo.demo.Service;

import com.example.demo.demo.Exception.ProduitNotFoundException;
import com.example.demo.demo.Model.Produit;
import com.example.demo.demo.Repository.ProduitRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.util.List;

@Service
public class ProduitService implements Serializable {

    private final ProduitRepository produitRepository;


    public ProduitService(ProduitRepository produitRepository) {
        this.produitRepository = produitRepository;
    }

    public Produit addProduit(Produit produit) {
        return produitRepository.save(produit);
    }

    public List<Produit> findAllProduits() { return produitRepository.findAll(); }

    public Produit updateProduit(Produit produit) {
        return produitRepository.save(produit);
    }

    public  Produit findProduitById(Long id) {
        return produitRepository.findProduitById(id)
                .orElseThrow(() -> new ProduitNotFoundException("Le produit " + id + " n'a pas été trouvé dans la base de données."));
    }

    @Transactional
    public void deleteProduit(Long id) {
        produitRepository.deleteProduitById(id);
    }

}
