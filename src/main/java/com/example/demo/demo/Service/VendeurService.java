package com.example.demo.demo.Service;

import com.example.demo.demo.Exception.ProduitNotFoundException;
import com.example.demo.demo.Model.Vendeur;
import com.example.demo.demo.Repository.VendeurRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class VendeurService {

    private final VendeurRepository vendeurRepository;

    public VendeurService(VendeurRepository vendeurRepository) {
        this.vendeurRepository = vendeurRepository;
    }

    public Vendeur addVendeur(Vendeur vendeur) {
        return vendeurRepository.save(vendeur);
    }

    public List<Vendeur> findAllVendeurs() { return vendeurRepository.findAll(); }

    public Vendeur updateVendeur(Vendeur vendeur) {
        return vendeurRepository.save(vendeur);
    }

    public  Vendeur findVendeurById(Long id) {
        return vendeurRepository.findVendeurById(id)
                .orElseThrow(() -> new ProduitNotFoundException("Le vendeur " + id + " n'a pas été trouvé dans la base de données."));
    }

    @Transactional
    public void deleteVendeur(Long id) {
        vendeurRepository.deleteVendeurById(id);
    }


}
