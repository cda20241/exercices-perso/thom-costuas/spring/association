package com.example.demo.demo.Model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.SelectBeforeUpdate;

import java.util.List;

@Entity
@Table(name = "produit")
@DynamicUpdate
@SelectBeforeUpdate
public class Produit {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="produit_id", nullable = false, updatable = false)
    private Long id;

    @Column(length=50)
    private String nom;

    @Column(length = 150)
    private String description;

    private Integer prix;

    @ManyToOne(fetch =  FetchType.EAGER)
    @JoinTable(name="vendeur_produits",joinColumns = @JoinColumn(name = "produits_produit_id", referencedColumnName = "produit_id"))
    private Vendeur vendeur;

    public Produit(Long id, String nom, String description, Integer prix, Vendeur vendeur) {
        this.id = id;
        this.nom = nom;
        this.description = description;
        this.prix = prix;
        this.vendeur = vendeur;
    }


    public Produit() {

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getPrix() {
        return prix;
    }

    public void setPrix(Integer prix) {
        this.prix = prix;
    }

    public Vendeur getVendeur() {
        return vendeur;
    }

    public void setVendeur(Vendeur vendeur) {
        this.vendeur = vendeur;
    }

    @Override
    public String toString() {
        return "Produit{" +
                "id=" + id +
                ", nom='" + nom + '\'' +
                ", description='" + description + '\'' +
                ", prix=" + prix +
                ", vendeur=" + vendeur +
                '}';
    }
}
