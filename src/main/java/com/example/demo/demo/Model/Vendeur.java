package com.example.demo.demo.Model;

import jakarta.persistence.*;

import java.util.List;

@Entity
@Table(name="vendeur")
public class Vendeur {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="vendeur_id", nullable = false, updatable = false)
    private Long id;

    @Column(length = 50)
    private String nom;

    @Column(length = 50)
    private String ville;

    @OneToMany(fetch = FetchType.EAGER, targetEntity = Produit.class, cascade = CascadeType.ALL)
    @JoinTable(name="vendeur_produits",joinColumns = @JoinColumn(name = "vendeur_vendeur_id", referencedColumnName = "vendeur_id"))
    private List<Produit> produits;

    public Vendeur(Long id, String nom, String ville, List<Produit> produits) {
        this.id = id;
        this.nom = nom;
        this.ville = ville;
        this.produits = produits;
    }

    public Vendeur() {

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getVille() {
        return ville;
    }

    public void setVille(String ville) {
        this.ville = ville;
    }

    public List<Produit> getProduits() {
        return produits;
    }

    public void setProduits(List<Produit> produits) {
        this.produits = produits;
    }

    @Override
    public String toString() {
        return "Vendeur{" +
                "id=" + id +
                ", nom='" + nom + '\'' +
                ", ville='" + ville + '\'' +
                ", produits=" + produits +
                '}';
    }
}
