package com.example.demo.demo.Controller;

import com.example.demo.demo.Model.Vendeur;
import com.example.demo.demo.Service.VendeurService;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(VendeurController.class)
public class VendeurControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private VendeurService vendeurService;

    @Test
    public void testGetVendeurs() throws Exception {
        Vendeur vendeur = new Vendeur(1L, "Vendeur1", "Ville1", null);
        Mockito.when(vendeurService.findAllVendeurs()).thenReturn(Arrays.asList(vendeur));

        mockMvc.perform(get("/vendeur/all"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].nom").value("Vendeur1"));
    }

    @Test
    public void testAddVendeur() throws Exception {
        Vendeur vendeur = new Vendeur(1L, "Vendeur1", "Ville1", null);
        Mockito.when(vendeurService.addVendeur(Mockito.any())).thenReturn(vendeur);

        mockMvc.perform(post("/vendeur/add")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"nom\":\"Vendeur1\",\"ville\":\"Ville1\"}"))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.nom").value("Vendeur1"));
    }

    @Test
    public void testGetVendeurById() throws Exception {
        Vendeur vendeur = new Vendeur(1L, "Vendeur1", "Ville1", null);
        Mockito.when(vendeurService.findVendeurById(1L)).thenReturn(vendeur);

        mockMvc.perform(get("/vendeur/find/1"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.nom").value("Vendeur1"));
    }

    @Test
    public void testUpdateVendeur() throws Exception {
        Vendeur vendeur = new Vendeur(1L, "Vendeur1", "Ville1", null);
        Mockito.when(vendeurService.updateVendeur(Mockito.any())).thenReturn(vendeur);

        mockMvc.perform(put("/vendeur/update")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"id\":1,\"nom\":\"Vendeur1\",\"ville\":\"Ville1\"}"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.nom").value("Vendeur1"));
    }

    @Test
    public void testDeleteVendeur() throws Exception {
        Mockito.doNothing().when(vendeurService).deleteVendeur(1L);

        mockMvc.perform(delete("/vendeur/delete/1"))
                .andExpect(status().isOk());
    }
}
