package com.example.demo.demo.Controller;

import com.example.demo.demo.Model.Produit;
import com.example.demo.demo.Service.ProduitService;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;
import java.util.Optional;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(ProduitController.class)
public class ProduitControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ProduitService produitService;

    @Test
    public void testGetProduits() throws Exception {
        Produit produit = new Produit(1L, "Produit1", "Description1", 100, null);
        Mockito.when(produitService.findAllProduits()).thenReturn(Arrays.asList(produit));

        mockMvc.perform(get("/produit/all"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].nom").value("Produit1"));
    }

    @Test
    public void testAddProduit() throws Exception {
        Produit produit = new Produit(1L, "Produit1", "Description1", 100, null);
        Mockito.when(produitService.addProduit(Mockito.any())).thenReturn(produit);

        mockMvc.perform(post("/produit/add")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"nom\":\"Produit1\",\"description\":\"Description1\",\"prix\":100}"))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.nom").value("Produit1"));
    }

    @Test
    public void testGetProduitById() throws Exception {
        Produit produit = new Produit(1L, "Produit1", "Description1", 100, null);
        Mockito.when(produitService.findProduitById(1L)).thenReturn(produit);

        mockMvc.perform(get("/produit/find/1"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.nom").value("Produit1"));
    }

    @Test
    public void testUpdateProduit() throws Exception {
        Produit produit = new Produit(1L, "Produit1", "Description1", 100, null);
        Mockito.when(produitService.updateProduit(Mockito.any())).thenReturn(produit);

        mockMvc.perform(put("/produit/update")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"id\":1,\"nom\":\"Produit1\",\"description\":\"Description1\",\"prix\":100}"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.nom").value("Produit1"));
    }

    @Test
    public void testDeleteProduit() throws Exception {
        Mockito.doNothing().when(produitService).deleteProduit(1L);

        mockMvc.perform(delete("/produit/delete/1"))
                .andExpect(status().isOk());
    }
}
