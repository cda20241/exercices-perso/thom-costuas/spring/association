package com.example.demo.demo.Service;

import com.example.demo.demo.Exception.ProduitNotFoundException;
import com.example.demo.demo.Model.Vendeur;
import com.example.demo.demo.Repository.VendeurRepository;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
public class VendeurServiceTest {

    @Autowired
    private VendeurService vendeurService;

    @MockBean
    private VendeurRepository vendeurRepository;

    @Test
    public void testAddVendeur() {
        Vendeur vendeur = new Vendeur(1L, "Vendeur1", "Ville1", null);
        Mockito.when(vendeurRepository.save(vendeur)).thenReturn(vendeur);

        Vendeur savedVendeur = vendeurService.addVendeur(vendeur);

        assertEquals("Vendeur1", savedVendeur.getNom());
    }

    @Test
    public void testFindAllVendeurs() {
        Vendeur vendeur = new Vendeur(1L, "Vendeur1", "Ville1", null);
        Mockito.when(vendeurRepository.findAll()).thenReturn(Arrays.asList(vendeur));

        List<Vendeur> vendeurs = vendeurService.findAllVendeurs();

        assertEquals(1, vendeurs.size());
        assertEquals("Vendeur1", vendeurs.get(0).getNom());
    }

    @Test
    public void testUpdateVendeur() {
        Vendeur vendeur = new Vendeur(1L, "Vendeur1", "Ville1", null);
        Mockito.when(vendeurRepository.save(vendeur)).thenReturn(vendeur);

        Vendeur updatedVendeur = vendeurService.updateVendeur(vendeur);

        assertEquals("Vendeur1", updatedVendeur.getNom());
    }

    @Test
    public void testFindVendeurById() {
        Vendeur vendeur = new Vendeur(1L, "Vendeur1", "Ville1", null);
        Mockito.when(vendeurRepository.findVendeurById(1L)).thenReturn(Optional.of(vendeur));

        Vendeur foundVendeur = vendeurService.findVendeurById(1L);

        assertEquals("Vendeur1", foundVendeur.getNom());
    }

    @Test
    public void testDeleteVendeur() {
        vendeurService.deleteVendeur(1L);
        Mockito.verify(vendeurRepository, Mockito.times(1)).deleteVendeurById(1L);
    }

    @Test
    public void testFindVendeurByIdNotFound() {
        Mockito.when(vendeurRepository.findVendeurById(1L)).thenReturn(Optional.empty());

        Exception exception = assertThrows(ProduitNotFoundException.class, () -> {
            vendeurService.findVendeurById(1L);
        });

        String expectedMessage = "Le vendeur 1 n'a pas été trouvé dans la base de données.";
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }
}
