package com.example.demo.demo.Service;

import com.example.demo.demo.Exception.ProduitNotFoundException;
import com.example.demo.demo.Model.Produit;
import com.example.demo.demo.Repository.ProduitRepository;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
public class ProduitServiceTest {

    @Autowired
    private ProduitService produitService;

    @MockBean
    private ProduitRepository produitRepository;

    @Test
    public void testAddProduit() {
        Produit produit = new Produit(1L, "Produit1", "Description1", 100, null);
        Mockito.when(produitRepository.save(produit)).thenReturn(produit);

        Produit savedProduit = produitService.addProduit(produit);

        assertEquals("Produit1", savedProduit.getNom());
    }

    @Test
    public void testFindAllProduits() {
        Produit produit = new Produit(1L, "Produit1", "Description1", 100, null);
        Mockito.when(produitRepository.findAll()).thenReturn(Arrays.asList(produit));

        List<Produit> produits = produitService.findAllProduits();

        assertEquals(1, produits.size());
    }

    @Test
    public void testUpdateProduit() {
        Produit produit = new Produit(1L, "Produit1", "Description1", 100, null);
        Mockito.when(produitRepository.save(produit)).thenReturn(produit);

        Produit updatedProduit = produitService.updateProduit(produit);

        assertEquals("Produit1", updatedProduit.getNom());
    }

    @Test
    public void testFindProduitById() {
        Produit produit = new Produit(1L, "Produit1", "Description1", 100, null);
        Mockito.when(produitRepository.findProduitById(1L)).thenReturn(Optional.of(produit));

        Produit foundProduit = produitService.findProduitById(1L);

        assertEquals("Produit1", foundProduit.getNom());
    }

    @Test
    public void testDeleteProduit() {
        produitService.deleteProduit(1L);
        Mockito.verify(produitRepository, Mockito.times(1)).deleteProduitById(1L);
    }
}
